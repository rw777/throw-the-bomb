let settings = new Phaser.Scene('settings');

var theBall;
var check;
var leftbtn, rightbtn, choosebtn;

var settingsManager;

settings.preload = function () {
    this.load.setBaseURL('assets');
    this.load.image('backButton', 'backButton.png');
    this.load.image('leftButton', 'leftButton.png');
    this.load.image('rightButton', 'rightButton.png');
    this.load.image('chooseButton', 'chooseButton.png');
    this.load.image('chosenButton', 'chosenButton.png');

    this.load.image('check', 'check.png');
    this.load.image('cloud', 'cloud.png');
    this.load.image('angryBall', 'madBasketball.png');

    this.load.image(models[0], 'basketball.png');
    this.load.image(models[1], 'colorBall.png');
    this.load.image(models[2], 'volleyBall.png');
    this.load.image(models[3], 'soccerBall.png');
    this.load.image(models[4], 'colorVolleyBall.png');
}

settings.create = function () {
    settingsManager = this;

    menuAudio.stop();

    backButton = this.add.image(config.width - 100, 50, 'backButton');
    leftbtn = this.add.image(config.width / 2 - 200, config.height / 2, 'leftButton').setVisible(false);
    rightbtn = this.add.image(config.width / 2 + 200, config.height / 2, 'rightButton');
    choosebtn = this.add.image(config.width / 2, config.height / 2 + 150, 'chosenButton');

    index = 0;
    pickedIndex = 0;

    theBall = this.add.image(config.width / 2, config.height / 2, models[index]).setScale(0.03);

    check = this.add.image(config.width / 2, config.height / 2, 'check').setScale(0.25).setVisible(true).setDepth(1);

    leftbtn.setInteractive();
    rightbtn.setInteractive();
    backButton.setInteractive();
    choosebtn.setInteractive();

    leftbtn.on('pointerup', function () {
        if (index > 0)
            move(0);
    });

    rightbtn.on('pointerup', function () {
        if (index < models.length - 1)
            move(1);
    });

    backButton.on('pointerup', function () {
        this.scene.scene.stop('settings');
        this.scene.scene.launch('menu');
    });

    choosebtn.on('pointerup', function () {
        chooseBall();
    });


}

function move(right) {
    check.setVisible(false);
    choosebtn = settingsManager.add.image(config.width / 2, config.height / 2 + 150, 'chooseButton');
    if (right == 0)
        index--;
    else
        index++;
    theBall.destroy();
    if (index == 0)
        theBall = settingsManager.add.image(config.width / 2, config.height / 2, models[index]).setScale(0.035);
    else if (index == 1)
        theBall = settingsManager.add.image(config.width / 2, config.height / 2, models[index]).setScale(0.03);
    else if (index == 2)
        theBall = settingsManager.add.image(config.width / 2, config.height / 2, models[index]).setScale(0.15);
    else if (index == 3)
        theBall = settingsManager.add.image(config.width / 2, config.height / 2, models[index]).setScale(0.13);
    else
        settingsManager.add.image(config.width / 2, config.height / 2, models[index]).setScale(0.28);
    rightbtn.setVisible(true);
    leftbtn.setVisible(true);
    if (index == 0) {
        leftbtn.setVisible(false);
    }
    if (index == models.length - 1) {
        rightbtn.setVisible(false);
    }
    if(index == pickedIndex){
        chooseBall();
    }
        
}

function chooseBall() {
    check.setVisible(true);
    pickedIndex = index;
    choosebtn = settingsManager.add.image(config.width / 2, config.height / 2 + 150, 'chosenButton');
}