var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: '#a3ecff',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 200 },
            debug: false
        }
    },
    scene: [menu,main,over,settings]
};

var game = new Phaser.Game(config);
