let over = new Phaser.Scene('over');

over.preload = function () {
    this.load.setBaseURL('assets');
    this.load.image('restartBtn', 'playAgain.png');
    this.load.image('mainmenu', 'mainMenu.png');
}

over.create = function () {
    this.add.text(config.width / 10, config.height / 5,
        'GAME OVER',
        { fontFamily: "Arial Black", fontSize: 100, color: "#e59090", align: 'center' });
    this.add.text(config.width / 3.5, config.height / 2.5,'Score: ' + score,
        { fontFamily: "Arial Black", fontSize: 70, color: "#000000", align: 'center' });
    var reBtn = this.add.image(config.width / 4 + 50, config.height - 150, 'restartBtn');
    var mainMenuBtn = this.add.image(config.width / 4 * 3 - 50, config.height - 150, 'mainmenu');

    reBtn.setInteractive();
    mainMenuBtn.setInteractive();

    reBtn.on('pointerup', function () {
        this.scene.scene.stop('over');
        this.scene.scene.launch('main');
    });

    mainMenuBtn.on('pointerup', function () {
        gameAudio.stop();
        this.scene.scene.stop('over');
        this.scene.scene.launch('menu');
    });
}
