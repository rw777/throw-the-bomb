let menu = new Phaser.Scene('menu');

var models = new Array();
var index, pickedIndex;
var menuAudio;


menu.preload = function () {
    models[0] = 'basketball';
    models[1] = 'colorSoccer';
    models[2] = 'volleyBall';
    models[3] = 'soccer';
    models[4] = 'colorVolley';

    this.load.setBaseURL('assets');
    this.load.image('playButton', 'playButton.png');
    this.load.image('settingsButton', 'settingsButton.png');
    this.load.image('cloud', 'cloud.png');
    this.load.image('angryBall', 'madBasketball.png');
    this.load.image('basketball', 'basketball.png');
    this.load.image('volleyBall', 'volleyBall.png');
    this.load.image('soccer', 'soccerBall.png');
    this.load.image('colorSoccer', 'colorfulBall.png');
    this.load.audio('menuSong','menuSong.wav');

}

menu.create = function () {
    menuAudio = this.sound.add('menuSong',
    {
        mute: false,
        volume: 1,
        rate: 1,
        detune: 0,
        seek: 0,
        loop: true,
        delay: 0
    });

    menuAudio.play();

    this.add.image(config.width / 5, config.height / 3, 'angryBall').setScale(0.3);

    var title = this.add.text(config.width / 3.5, config.height / 3.5, "Bash the Crate", { fontFamily: "Arial Black", fontSize: 50, color: "#f4e242" });
    title.setStroke('#d19306', 16);

    var playBtn = this.add.image(config.width / 2, config.height / 2 + 50, 'playButton');
    var setBtn = this.add.image(config.width / 2, config.height / 1.5 + 50, 'settingsButton');

    if (typeof index === 'undefined')
        index = 0;

    if (typeof pickedIndex === 'undefined')
        pickedIndex = 0;

    playBtn.scaleX = 1.05;

    playBtn.setInteractive();
    setBtn.setInteractive();

    playBtn.on('pointerup', function () {
        menuAudio.setLoop(false);
        menuAudio.stop();
        this.scene.scene.stop('menu');
        this.scene.scene.launch('main');
    });

    setBtn.on('pointerup', function () {
        this.scene.scene.stop('menu');
        this.scene.scene.launch('settings');
    });
}
