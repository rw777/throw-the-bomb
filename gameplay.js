let main = new Phaser.Scene('main');

var score, scoreText, addScore;

var ground;

var ball;
var ballX, ballY;

var hoop;
var crate;
var bottomBorder;
var bin;
var targetX, targetY;

var velocity;

var isLaunched;

var sceneManager;

var betweenPoints;
var graphics;
var vector;
var line;
var setToAngle;
var vectorFromRotation;

var swish, gameAudio;


main.preload = function () {
    this.load.setBaseURL('assets');

    var progress = this.add.graphics();
    var progressBar = this.add.graphics();
    progressBar.fillStyle(0x222222, 0.8);
    progressBar.fillRect(240, 270, 320, 50);

    var i;
    for (i = 0; i < 1000; i += 3) {
        i--;
    }

    this.load.image('ball', 'basketball.png');
    this.load.image('crate', 'crate.png');
    this.load.image('ground', 'ground.png');
    this.load.image('vert', 'verticalBorder.png');
    this.load.image('hori', 'horizontalBorder.png');
    this.load.image('cap', 'cap.png');
    this.load.image('sky', 'sky.png');
    this.load.audio('gameSong', 'gameSong.mp3');
    this.load.audio('whoosh', 'whoosh.mp3');
    this.load.audio('oh', 'oh.wav');

    this.load.on('progress', (x) => {
        progress.clear();
        progress.fillStyle(0x5544ff, 1);
        progress.fillRect(250, 280, 300 * x, 30);
    });
}

/*
    Mengcreate permainan
*/
main.create = function () {
    sceneManager = this;
    gameAudio = this.sound.add('gameSong', {
        mute: false,
        volume: 1,
        rate: 1,
        detune: 0,
        seek: 0,
        loop: true,
        delay: 0
    });
    gameAudio.play();
    swish = this.sound.add('whoosh');
    sceneManager.add.image(350, 300, 'sky');
    reset(sceneManager);
    score = 0;
    scoreText = sceneManager.add.text(config.width - 120, 16, '000000', { fontFamily: 'Arial', fontSize: '30px', fill: "#000000" });
}

function reset(sceneManager) {
    isLaunched = false

    ballX = 100;
    ballY = config.height - 102;

    targetX = 500 + Math.floor((Math.random() * 180));
    targetY = config.height - 110;

    addScore = sceneManager.add.text(targetX, targetY, '+1', { fontSize: '30px', fill: "#000000" }).setVisible(false);

    graphics = sceneManager.add.graphics().setDefaultStyles({ lineStyle: { width: 10, color: 0x00ddff, alpha: 0.5 } });

    betweenPoints = Phaser.Math.Angle.Between;
    setToAngle = Phaser.Geom.Line.SetToAngle;
    vectorFromRotation = sceneManager.physics.velocityFromRotation;

    vector = new Phaser.Math.Vector2();
    line = new Phaser.Geom.Line();

    getBall();

    sceneManager.input.on('pointerup', () => launchObject(sceneManager.input.x, sceneManager.input.y));

    ball.setCollideWorldBounds(true);

    ball.body.onWorldBounds = true;
    ball.body.world.on('worldbounds', function (body) {
        displayGameOver();
    }, sceneManager);

    ground = sceneManager.physics.add.staticGroup();

    ground.create(400, 568, 'ground').setScale(2.3).setVisible(false).refreshBody();

    sceneManager.physics.add.collider(ball, ground, function () {
        checkGameOver();
    });

    bin = sceneManager.physics.add.staticGroup();

    sceneManager.physics.add.collider(ball, bin, () => {
        ball.body.angularVelocity = 10;
        ball.setAngularAcceleration(-100);
    });

    //rim
    bin.create(targetX, targetY - 11, 'vert').setSize(5, 100).setVisible(false);
    bin.create(targetX + 95, targetY - 11, 'vert').setSize(5, 100).setVisible(false);

    bottomBorder = sceneManager.physics.add.image(targetX + 50, config.height - 110, 'hori').setSize(100, 30).setVisible(false);

    sceneManager.physics.add.collider(bottomBorder, ground);

    sceneManager.physics.add.collider(ball, bottomBorder, () => {
        updateScore(sceneManager);
    });

    crate    = sceneManager.add.image(targetX + 50, targetY, 'crate').setScale(0.2);
}

main.update = function () {
    if (this.input.activePointer.isDown) {
        aim(ball.x, ball.y, this.input.x, this.input.y);
    }
}

function resetScore() {
    score = 0;
    this.scoreText.setText("Score: " + this.score);
}

function aim(x1, y1, x2, y2) {
    if (!isLaunched) {
        var angle = betweenPoints(x1, y1, x2, y2);
        var dist = Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));
        if (dist > 500)
            dist = 500;
        setToAngle(line, ball.x, ball.y, angle, dist);
        vectorFromRotation(angle, 600, vector);
        graphics.clear().strokeLineShape(line);
    }
}


function launchObject(x, y) {
    if (!isLaunched) {
        graphics.clear();
        ball.setBounce(1);
        ball.setAcceleration(-1, 10);
        if (x > 400)
            x = 400;
        if (y > 300)
            y = 300;
        ball.setVelocity(x - ball.x, y - ball.y);
        ball.body.angularVelocity = 700;
        ball.setAngularAcceleration(-150);
        isLaunched = true;
    }
}

/*
    Menampilkan layar game over
*/
function displayGameOver() {
    var oh = sceneManager.sound.add('oh');
    oh.play();
    sceneManager.scene.stop();
    sceneManager.scene.start('over');
}

function updateScore(sceneManager) {
    swish.play();
    addScore.setVisible(true);
    sceneManager.tweens.add({
        targets: addScore,
        y: targetY - 100,
        alpha: 0,
        duration: 500,
        ease: 'Power2'
    }, this);
    var i;
    for (i = 0; i <= 10000; i++) {
        if (i == 1)
            score += i;
    }
    if (score >= 100000)
        this.scoreText.setText(this.score);
    else if (score >= 10000)
        this.scoreText.setText('0' + this.score);
    else if (score >= 1000)
        this.scoreText.setText('00' + this.score);
    else if (score >= 100)
        this.scoreText.setText('000' + this.score);
    else if (score >= 10)
        this.scoreText.setText('0000' + this.score);
    else
        this.scoreText.setText('00000' + this.score);
    
    this.ball.destroy();
    this.crate.destroy();
    this.bin.children.each(function (b) {
        b.destroy();
    }, this);
    this.bottomBorder.destroy();
    reset(sceneManager);
}

function checkGameOver() {
    if (isLaunched) {
        displayGameOver();
    }
}

function getBall() {
    ball = sceneManager.physics.add.image(ballX, ballY, models[index]);
    if (index == 0) {
        ball.setScale(0.015);
        ball.setCircle(1750, 0, 0);
    }
    else if (index == 1) {
        ball.setScale(0.013);
        ball.setCircle(2000, 0, 0);
    }
    else if (index == 2) {
        ball.setScale(0.06);
        ball.setCircle(400, 0, 0);
    }
    else if (index == 3) {
        ball.setScale(0.06);
        ball.setCircle(450, 5, 3);
    }
    else {
        ball.setScale(0.12);
        ball.setCircle(225, 10, 7);
    }

}